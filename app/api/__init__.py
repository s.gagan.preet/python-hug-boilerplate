from auth import create_api as auth_api
from home import create_api as home_api

def create_api(hug):
	auth_api(hug)
	home_api(hug)
